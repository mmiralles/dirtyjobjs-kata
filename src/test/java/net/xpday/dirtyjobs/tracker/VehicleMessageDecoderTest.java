package net.xpday.dirtyjobs.tracker;

import static org.mockito.Mockito.when;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class VehicleMessageDecoderTest
{
    // @Mock
    @Test
    public void retrieveColor() throws NumberFormatException,
            RemoteException,
            MalformedURLException,
            NotBoundException
    {

        VehicleTrackingService trackingService = Mockito.mock(VehicleTrackingService.class);
        String vehicleHost = "localhost";
        MessageDecoder decoder = null;
        when(trackingService.trackVehicle(vehicleHost, decoder)).thenReturn(new Integer("01"));

        byte decoderId = (byte) 0x02;
        VehicleMessageDecoder vmd = new VehicleMessageDecoder(vehicleHost, decoderId);
        Assert.assertEquals("Should be 01", 01, vmd.getColor());
    }
}
