/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;


public class AbstractVehicleFieldPanel extends JPanel {
	private class Canvas implements VehicleCanvas {

		private Graphics graphics;

		public Canvas(Graphics g) {
			this.graphics = g;
		}


		public void fillSquare(int x, int y, int size, int color) {
			graphics.setColor(new Color(color));
			graphics.fillRect(x, y, size, size);
			graphics.setColor(Color.BLACK);
			graphics.drawRect(x, y, size, size);

		}

	}


	public AbstractVehicleFieldPanel() {
		super();
		setBorder(BorderFactory.createLineBorder(Color.black));
	}

	public void redraw() {
		repaint();
		repaint();
	}
	
	protected VehicleCanvas canvas(Graphics g) {
		return new Canvas(g);
	}
}