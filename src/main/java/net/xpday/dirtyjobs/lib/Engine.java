/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

/**
 * The engine status enum class
 */
public class Engine {
	private boolean on;
	private Engine(boolean b) {
		on = b;
	}

	public static final Engine On = new Engine(true);
	public static final Engine Off = new Engine(false);

	@Override
	public String toString() {
		return "Engine " + (on ? "On" : "Off");
	}

	public boolean on() {
		return on;
	}
}
