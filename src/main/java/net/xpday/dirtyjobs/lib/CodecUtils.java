/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

/**
 * A collection of static utilities to encode messages 
 * (see <ul>
 * <li>{@link #createFullFrame(byte, int, int, int, Direction, Speed, Engine, CloseToOther, Collision)}, </li>
 * <li>{@link #createClosingInOnOthersStatusFrame(byte, int, CloseToOther)}, </li>
 * <li>{@link #createCollisionStatusFrame(byte, int, Collision)}, </li>
 * <li>{@link #createDirectionFrame(byte, int, Direction)}, </li>
 * <li>{@link #createPositionFrame(byte, int, int, int)}, </li>
 * <li>{@link #createSpeedFrame(byte, int, Speed)}, </li>
 * </ul>
 * 
 * And a collection of lower level codec utils like <ul>
 * <li>{@link #decodeSpeed(byte[], int)}, </li>
 * <li>{@link #encodeSpeed(Speed, byte[], int)}, </li>
 * <li>{@link #encodeClosing(CloseToOther, byte[], int)}, </li>
 * <li>...and more..</li>
 * </ul>
 * 
 * @author rob
 *
 */
public class CodecUtils {
	protected static int unsign(byte b) {return b < 0 ? 256+b : b;}
	
	public static int decodeInt16(byte[] bytes, int index) {
		int msb = unsign(bytes[index]);
		int lsb = unsign(bytes[index + 1]);
		return (msb << 8) + lsb;
	}

	public static int decodeByte(byte[] bytes, int index) {
		return bytes[index];
	}

	public static int decodeInt32(byte[] bytes, int index) {
		return (unsign(bytes[index]) << 24) + (unsign(bytes[index + 1]) << 16)
				+ (unsign(bytes[index + 2]) << 8) + unsign(bytes[index + 3]);
	}

	public static void encodeInt16(int value, byte[] result, int index) {
		result[index] = (byte) ((value & 0xFF00) >> 8);
		result[index + 1] = (byte) (value & 0xFF);
	}

	public static void encodeInt32(int value, byte[] result, int index) {
		result[index] = (byte) ((value & 0xFF000000) >> 24);
		result[index + 1] = (byte) ((value & 0xFF0000) >> 16);
		result[index + 2] = (byte) ((value & 0xFF00) >> 8);
		result[index + 3] = (byte) (value & 0xFF);
	}

	public static Direction decodeDirection(byte[] result, int index) {
		if (result[index] < 0) return Direction.West;
		if (result[index+1] < 0) return Direction.North;
		if (result[index+1] > 0) return Direction.South;
		return Direction.East;
	}

	public static void encodeDirection(Direction direction, byte[] result, int index) {
		result[index] = (byte)direction.getXDisplacement(); 
		result[index+1] = (byte)direction.getYDisplacement(); 
	}

	public static Speed decodeSpeed(byte[] result, int index) {
		return new Speed(result[index]);
	}

	public static void encodeSpeed(Speed speed, byte[] result, int index) {
		result[index] = (byte)speed.getValue();
	}

	public static Engine decodeEngine(byte[] result, int index) {
		return result[index] == 0 ? Engine.Off : Engine.On;
	}

	public static void encodeEngine(Engine engineRunning, byte[] result,
			int index) {
		result[index] = (byte)(engineRunning.on()?1:0);
	}

	public static CloseToOther decodeClosing(byte[] result, int index) {
		return result[index] == 0 ? CloseToOther.No: CloseToOther.Yes;
	}

	public static void encodeClosing(CloseToOther closing, byte[] result,
			int index) {
		result[index] = (byte)(closing == CloseToOther.Yes ? 1:0);
	}

	public static Collision decodeCollision(byte[] result, int index) {
		return result[index] == 0 ? Collision.No: Collision.Yes;
	}

	public static void encodeCollision(Collision collision, byte[] result,
			int index) {
		result[index] = (byte)(collision == Collision.Yes ? 1:0);
	}

	/**
	 * Creates a full frame to be send to the tracker. 
	 * Frame is a byte array with the following format:
	 * <pre>
	 * |F|CRC|I|T|P|D|S|E|A|C|
	 * F Frametype			[1 bytes 0x00 = Full Frame]
	 * CRC                  [1 bytes crc32]
	 * T TimeStamp			[4 bytes unsigned long int Little Endian]
	 * I Identification     [1 byte identification]
	 * P Position			[4 bytes |X|Y| small int x, small int y]]
	 * D Direction     		[2 bytes [X|Y] 0|1 = south, 0|FF = north, 1|0 = east, FF|0 = west]
	 * S Speed				[1 bytes byte value]
	 * E EngineOn			[1 bytes byte value]
	 * A Dangerouslyclose   [1 bytes boolean (1 = true, 0 = false)]
	 * C Collision			[1 bytes boolean (1 = true, 0 = false)]
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param xpos
	 * @param ypos
	 * @param dir
	 * @param spd
	 * @param eon
	 * @param close
	 * @param coll
	 * @return
	 */
	public static byte[] createFullFrame(byte ident, int tstamp, int xpos, int ypos,
			Direction dir, Speed spd, Engine eon, CloseToOther close,
			Collision coll) {
		byte[] result = new byte[Protocol.FullFrameLength];
		result[0] = Protocol.FullFrame;
		result[Protocol.FrameLengthIndex] = Protocol.FullFrameLength;
		result[Protocol.IdentificationIndex] = ident; 
		encodeInt32(tstamp, result, Protocol.TimeStampIndex);
		encodeInt16(xpos, result, Protocol.XPosIndex);
		encodeInt16(ypos, result, Protocol.YPosIndex);
		encodeDirection(dir, result, Protocol.DirectionIndex);
		encodeSpeed(spd, result, Protocol.SpeedIndex);
		encodeEngine(eon, result, Protocol.EngineIndex);
		encodeClosing(close, result, Protocol.ClosingIndex);
		encodeCollision(coll, result, Protocol.CollisionIndex);
		result[Protocol.CrcIndex] = Crc8.checksum(result, Protocol.PayloadIndex, Protocol.FullFramePayload);
		return result;
	}

	public static byte[] deltaFrameFrom(byte[] data, byte frameType, int frameLength, byte ident, int tstamp) {
		byte[] result = new byte[frameLength];
		data[Protocol.FrameTypeIndex] = frameType;
		data[Protocol.FrameLengthIndex] = (byte)frameLength;
		data[Protocol.IdentificationIndex] = ident; 
		encodeInt32(tstamp, data, Protocol.TimeStampIndex);
		System.arraycopy(data, 0, result, 0, frameLength);
		result[Protocol.CrcIndex] = Crc8.checksum(result, Protocol.PayloadIndex, result.length - Protocol.PayloadIndex);
		return result;
	}

	/**
	 * Creates a frame indicating a changed position
	 * Frame format is a byte array containing:
	 * <pre>
	 * F|CRC|I|T|DDDD
	 * F Frametype			[1 bytes 0x01 = Position]
	 * T Timestamp
	 * I Identification
	 * D data (max 4 bytes)
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param xpos
	 * @param ypos
	 * @return
	 */
	public static byte[] createPositionFrame(byte ident, int tstamp, int xpos, int ypos) {
		byte[] data = new byte[16];
		encodeInt16(xpos, data, Protocol.XPosIndex);
		encodeInt16(ypos, data, Protocol.YPosIndex);
		byte[] frame = deltaFrameFrom(data,Protocol.PartialPositionFrame, Protocol.PartialPositionFrameLength, ident, tstamp);
		return frame;
	}

	/**
	 * Creates a frame indicating a change in direction
	 * <pre>
	 * F|CRC|I|T|DDDD
	 * F Frametype			[1 0x02 = Direction]
	 * T Timestamp
	 * I Identification
	 * D data (max 4 bytes)
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param dir
	 * @return
	 */
	public static byte[] createDirectionFrame(byte ident, int tstamp, Direction dir) {
		byte[] data = new byte[16];
		encodeDirection(dir,data, Protocol.TimeStampIndex+4);
		byte[] frame = deltaFrameFrom(data,Protocol.PartialDirectionFrame, Protocol.PartialDirectionFrameLength, ident, tstamp);
		return frame;
	}

	/**
	 * Creates a frame indicating a changed Close To Others Status
	 * Frame format is a byte array containing:
	 * <pre>
	 * F|CRC|I|T|DDDD
	 * F Frametype			[1 bytes 0x05 = Dangerouslyclose]
	 * T Timestamp
	 * I Identification
	 * D data (max 4 bytes)
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param close
	 * @return
	 */
	public static byte[] createClosingInOnOthersStatusFrame(byte ident, int tstamp,
			CloseToOther close) {
		byte[] data = new byte[16];
		encodeClosing(close, data, Protocol.TimeStampIndex+4);
		byte[] frame = deltaFrameFrom(data,Protocol.PartialClosingFrame, Protocol.PartialClosingFrameLength, ident, tstamp);
		return frame;
	}

	/**
	 * Creates a frame indicating a changed CollisionStatus
	 * Frame format is a byte array containing:
	 * <pre>
	 * F|CRC|I|T|DDDD
	 * F Frametype			[1 bytes 0x06 = Collision]
	 * T Timestamp
	 * I Identification
	 * D data (max 4 bytes)
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param coll
	 * @return
	 */
	public static byte[] createCollisionStatusFrame(byte ident, int tstamp,
			Collision coll) {
		byte[] data = new byte[16];
		encodeCollision(coll, data, Protocol.TimeStampIndex+4);
		byte[] frame = deltaFrameFrom(data,Protocol.PartialCollisionFrame, Protocol.PartialCollisionFrameLength, ident, tstamp);
		return frame;
	}

	/**
	 * Creates a frame indicating a changed Engine Status
	 * Frame format is a byte array containing:
	 * <pre>
	 * F|CRC|I|T|DDDD
	 * F Frametype			[1 bytes 0x04 = EngineOn]
	 * T Timestamp
	 * I Identification
	 * D data (max 4 bytes)
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param engine
	 * @return
	 */
	public static byte[] createEngineStatusFrame(byte ident, int tstamp, Engine engine) {
		byte[] data = new byte[16];
		encodeEngine(engine, data, Protocol.TimeStampIndex+4);
		byte[] frame = deltaFrameFrom(data,Protocol.PartialEngineFrame, Protocol.PartialEngineFrameLength, ident, tstamp);
		return frame;
	}

	/**
	 * Creates a frame indicating a changed Speed
	 * Frame format is a byte array containing:
	 * <pre>
	 * F|CRC|I|T|DDDD
	 * F Frametype			[1 0x03 = Speed]
	 * T Timestamp
	 * I Identification
	 * D data (max 4 bytes)
	 * </pre>
	 * @param ident
	 * @param tstamp
	 * @param spd
	 * @return
	 */
	public static byte[] createSpeedFrame(byte ident, int tstamp, Speed spd) {
		byte[] data = new byte[16];
		encodeSpeed(spd,data, Protocol.TimeStampIndex+4);
		byte[] frameFrom = deltaFrameFrom(data,Protocol.PartialSpeedFrame, Protocol.PartialSpeedFrameLength, ident, tstamp);
		return frameFrom;
	}

}
