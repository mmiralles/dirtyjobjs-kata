/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;


/** 
 * A position utility class 
 */
public class Position {

	private int xPos;
	private int yPos;

	public Position(int x, int y) {
		xPos = x;
		yPos = y;
	}

	public boolean equals(Object other) {
		if (!(other instanceof Position)) return false; 
		Position otherPosition = (Position)other;
		return xPos == otherPosition.xPos && yPos == otherPosition.yPos;
	}
	
	public String toString() {
		return "("+xPos+","+yPos+")";
	}
}
