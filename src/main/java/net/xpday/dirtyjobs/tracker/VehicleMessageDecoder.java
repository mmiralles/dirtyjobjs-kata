/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.tracker;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import net.xpday.dirtyjobs.lib.CodecUtils;
import net.xpday.dirtyjobs.lib.Protocol;
import net.xpday.dirtyjobs.tracker.TrackingCache.Entry;

public class VehicleMessageDecoder implements MessageDecoder
{

    private static final int WAITING_FOR_INITIALFRAME = 1;
    private static final int STARTING = 0;
    private static final int WAITING_FOR_DELTAS = 2;
    public static final int MAX_DELTAS = 4;
    private static final int LAST_DELTA = MAX_DELTAS - 1;
    private final byte decoderId;
    private Entry currentEntry;
    private int decodingState = 0;
    private int numberOfDeltas;

    public VehicleMessageDecoder(String vehicleHost, byte decoderId) throws RemoteException,
                                                                    MalformedURLException, NotBoundException
    {
        this.decoderId = decoderId;
        VehicleTrackingService.getInstance().trackVehicle(vehicleHost, this);
    }

    @Override
    public byte identification()
    {
        return decoderId;
    }

    @Override
    public void handleMessage(byte identification, byte[] message, int length)
    {
        if (identification != decoderId)
            return;
        switch (decodingState)
        {
            case STARTING:
                decodingState = WAITING_FOR_INITIALFRAME;
            case WAITING_FOR_INITIALFRAME:
                if (message[Protocol.FrameTypeIndex] == Protocol.FullFrame)
                {
                    currentEntry = TrackingCache.getInstance().createCacheEntry(decoderId);
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setXPos(CodecUtils.decodeInt16(message, Protocol.XPosIndex));
                    currentEntry.setYPos(CodecUtils.decodeInt16(message, Protocol.YPosIndex));
                    currentEntry.setDirection(CodecUtils.decodeDirection(message, Protocol.DirectionIndex));
                    currentEntry.setSpeed(CodecUtils.decodeSpeed(message, Protocol.SpeedIndex));
                    currentEntry.setEngineStatus(CodecUtils.decodeEngine(message, Protocol.EngineIndex));
                    currentEntry.setClosingStatus(CodecUtils.decodeClosing(message, Protocol.ClosingIndex));
                    currentEntry.setCollisionStatus(CodecUtils.decodeCollision(message,
                                                                               Protocol.CollisionIndex));
                    // inicializar color.
                    currentEntry.commit();
                    decodingState = WAITING_FOR_DELTAS;
                    numberOfDeltas = 0;
                }
                break;

            case WAITING_FOR_DELTAS:
                if (numberOfDeltas >= MAX_DELTAS)
                {
                    break;
                }
                if (numberOfDeltas >= LAST_DELTA)
                {
                    decodingState = WAITING_FOR_INITIALFRAME;
                }
                if (message[Protocol.FrameTypeIndex] == Protocol.FullFrame)
                {
                    for (int i = numberOfDeltas; i < MAX_DELTAS; i++)
                    {
                        currentEntry.commit();
                    }
                    decodingState = WAITING_FOR_INITIALFRAME;
                    handleMessage(identification, message, length);
                }
                if (message[Protocol.FrameTypeIndex] == Protocol.PartialPositionFrame)
                {
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setXPos(CodecUtils.decodeInt16(message, Protocol.XPosIndexInDelta));
                    currentEntry.setYPos(CodecUtils.decodeInt16(message, Protocol.YPosIndexInDelta));
                    currentEntry.commit();
                    numberOfDeltas++;
                }
                else if (message[Protocol.FrameTypeIndex] == Protocol.PartialDirectionFrame)
                {
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setDirection(CodecUtils.decodeDirection(message,
                                                                         Protocol.DirectionIndexInDelta));
                    currentEntry.commit();
                    numberOfDeltas++;
                }
                else if (message[Protocol.FrameTypeIndex] == Protocol.PartialSpeedFrame)
                {
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setSpeed(CodecUtils.decodeSpeed(message, Protocol.SpeedIndexInDelta));
                    currentEntry.commit();
                    numberOfDeltas++;
                }
                else if (message[Protocol.FrameTypeIndex] == Protocol.PartialEngineFrame)
                {
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setEngineStatus(CodecUtils
                            .decodeEngine(message, Protocol.EngineIndexInDelta));
                    currentEntry.commit();
                    numberOfDeltas++;
                }
                else if (message[Protocol.FrameTypeIndex] == Protocol.PartialClosingFrame)
                {
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setClosingStatus(CodecUtils.decodeClosing(message,
                                                                           Protocol.ClosingIndexInDelta));
                    currentEntry.commit();
                    numberOfDeltas++;
                }
                else if (message[Protocol.FrameTypeIndex] == Protocol.PartialCollisionFrame)
                {
                    currentEntry.setTimestamp(CodecUtils.decodeInt32(message, Protocol.TimeStampIndex));
                    currentEntry.setCollisionStatus(CodecUtils
                            .decodeCollision(message, Protocol.CollisionIndexInDelta));
                    currentEntry.commit();
                    numberOfDeltas++;
                }

            default:
                break;
        }

    }

    @Override
    public void tick()
    {
        // TODO - something useful
    }

    public Object getColor()
    {
        // TODO Auto-generated method stub
        return null;
    }

}
