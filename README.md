1. create de project jar with
'''bash
mvn package
'''

2. include thath package in the classpath and run rmiregistry
'''bash
export CLASSPATH=(paht_to_the_project)/target/dirtyjobs-0.0.1-SNAPSHOT.jar
rmiregistry
'''

3. run each of the following command on different terminals
'''bash
mvn exec:java -Dexec.mainClass=net.xpday.dirtyjobs.vehicles.VehicleFieldGui
'''
'''bash
mvn exec:java -Dexec.mainClass=net.xpday.dirtyjobs.tracker.TrackerGui -Dexec.args="localhost 4444 localhost"
'''
